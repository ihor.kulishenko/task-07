package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public class DBManager {

    private static final String APP_PROPS_FILE = "app.properties";
    private static DBManager instance;
    private final String connectionString;

    private DBManager() {
        try (final var inputStream = Files.newInputStream(Path.of(APP_PROPS_FILE))) {

            final var properties = new Properties();
            properties.load(inputStream);
            connectionString = properties.getProperty("connection.url");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static synchronized DBManager getInstance() {
        if (instance == null) {

            instance = new DBManager();
        }

        return instance;
    }

    private Connection getConnection() throws DBException {
        try {
            return DriverManager.getConnection(connectionString);
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
    }

    public List<User> findAllUsers() throws DBException {
        final var sql = "select id, login from USERS";

        try (final var conn = getConnection(); final var ps = conn.prepareStatement(sql)) {
            final var rs = ps.executeQuery();

            final List<User> result = new ArrayList<>();
            while (rs.next()) {
                final var user = new User();
                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));

                result.add(user);
            }

            return result;
        } catch (Exception e) {
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean insertUser(User user) throws DBException {

        final var sql = "insert into users (LOGIN) values ( ? )";

        try (final var conn = getConnection()) {

            conn.setAutoCommit(false);
            try (final var ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

                // insert new user
                ps.setString(1, user.getLogin());
                ps.executeUpdate();

                final var rs = ps.getGeneratedKeys();

                rs.next();
                user.setId(rs.getInt(1));
            } catch (Exception e) {
                conn.rollback();
                conn.setAutoCommit(true);
                throw new DBException(e.getMessage(), e);
            }

            conn.commit();
            conn.setAutoCommit(true);

            return true;
        } catch (Exception e) {
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        final var sql = "delete from USERS where ID = ?";

        try (final var conn = getConnection()) {

            conn.setAutoCommit(false);

            for (User user : users) {
                try (final var ps = conn.prepareStatement(sql)) {
                    ps.setInt(1, user.getId());
                    ps.executeUpdate();
                } catch (Exception e) {
                    conn.rollback();
                    conn.setAutoCommit(true);
                    throw new DBException(e.getMessage(), e);
                }
            }

            conn.commit();
            conn.setAutoCommit(true);

            return true;
        } catch (Exception e) {
            throw new DBException(e.getMessage(), e);
        }
    }

    public User getUser(String login) throws DBException {
        final var sql = "select * from USERS where LOGIN = ?";

        try (final var conn = getConnection();
             final var ps = conn.prepareStatement(sql)) {
            ps.setString(1, login);

            final var rs = ps.executeQuery();

            return rs.next() ? getUserFromRS(rs) : null;

        } catch (Exception e) {
            throw new DBException(e.getMessage(), e);
        }
    }

    public Team getTeam(String name) throws DBException {
        final var sql = "select * from TEAMS where NAME = ?";

        try (final var conn = getConnection();
             final var ps = conn.prepareStatement(sql)) {
            ps.setString(1, name);

            final var rs = ps.executeQuery();

            return rs.next() ? getTeamFromRS(rs) : null;

        } catch (Exception e) {
            throw new DBException(e.getMessage(), e);
        }
    }

    public List<Team> findAllTeams() throws DBException {
        final var sql = "select id, name from TEAMS";

        try (final var conn = getConnection();
             final var ps = conn.prepareStatement(sql)) {

            final var rs = ps.executeQuery();

            return getTeamsFromRS(rs);
        } catch (Exception e) {
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean insertTeam(Team team) throws DBException {
        final var sql = "insert into TEAMS (NAME) values ( ? )";

        try (final var conn = getConnection();
             final var ps = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

            ps.setString(1, team.getName());
            ps.executeUpdate();

            final var rs = ps.getGeneratedKeys();
            if (!rs.next()) {
                return false;
            }

            team.setId(rs.getInt(1));

            return true;

        } catch (Exception e) {
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {

        final var sql = "insert into USERS_TEAMS values ( ?, ? )";

        try (final var conn = getConnection()) {
            conn.setAutoCommit(false);

            for (Team team : teams) {
                try (final var ps = conn.prepareStatement(sql)) {

                    ps.setInt(1, user.getId());
                    ps.setInt(2, team.getId());
                    ps.executeUpdate();

                } catch (Exception e) {
                    conn.rollback();
                    conn.setAutoCommit(true);
                    throw new DBException(e.getMessage(), e);
                }
            }

            conn.commit();
            conn.setAutoCommit(true);

            return true;

        } catch (Exception e) {
            throw new DBException(e.getMessage(), e);
        }
    }

    public List<Team> getUserTeams(User user) throws DBException {
        final var sql = "select t.ID as id, t.NAME as name " +
                "from TEAMS t, USERS_TEAMS ut " +
                "where t.ID = ut.TEAM_ID and ut.USER_ID = ?";

        try (final var conn = getConnection();
             final var ps = conn.prepareStatement(sql)) {

            ps.setInt(1, user.getId());

            final var rs = ps.executeQuery();

            return getTeamsFromRS(rs);

        } catch (Exception e) {
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean deleteTeam(Team team) throws DBException {
        final var sql = "delete from TEAMS where ID = ?";

        try (final var conn = getConnection()) {

            conn.setAutoCommit(false);

            try (final var ps = conn.prepareStatement(sql)) {
                ps.setInt(1, team.getId());

                ps.executeUpdate();
            } catch (Exception e) {
                conn.rollback();
                conn.setAutoCommit(true);

                throw new DBException(e.getMessage(), e);
            }

            conn.commit();
            conn.setAutoCommit(true);

            return true;
        } catch (Exception e) {
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        final var sql = "update TEAMS set NAME = ? where ID = ?";

        try (final var conn = getConnection()) {

            conn.setAutoCommit(false);

            try (final var ps = conn.prepareStatement(sql)) {
                ps.setString(1, team.getName());
                ps.setInt(2, team.getId());

                ps.executeUpdate();
            } catch (Exception e) {
                conn.rollback();
                conn.setAutoCommit(true);

                throw new DBException(e.getMessage(), e);
            }

            conn.commit();
            conn.setAutoCommit(true);

            return true;
        } catch (Exception e) {
            throw new DBException(e.getMessage(), e);
        }
    }

    private User getUserFromRS(ResultSet rs) throws SQLException {
        final var user = new User();

        user.setId(rs.getInt("id"));
        user.setLogin(rs.getString("login"));

        return user;
    }

    private Team getTeamFromRS(ResultSet rs) throws SQLException {
        final var team = new Team();

        team.setId(rs.getInt("id"));
        team.setName(rs.getString("name"));

        return team;
    }

    private List<Team> getTeamsFromRS(ResultSet rs) throws SQLException {
        final var result = new ArrayList<Team>();

        while (rs.next()) {
            result.add(getTeamFromRS(rs));
        }

        return result;
    }

}
